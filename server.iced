http = require 'http'
fs = require 'fs'


server = http.createServer (req, res) ->
    #console.log req.method, req.url
    
    requestedMethod = req.url[1..]
    requestedFormats = req.headers.accept.split ','

    try
        if requestedMethod isnt 'check' 
            throw 'unknown request method: ' + requestedMethod
        await @doCheck defer result, reason
        @writeResponse res, 200, @formatResponse(result, reason, requestedFormats)... 
    catch error
        code = if error.match /unknown request method/i then 404 else 500
        @writeResponse res, code, 'text/plain', error,

server.doCheck = (cb) ->
    await fs.readFile 'checks.json', defer fileError, jsonData
    if fileError? then throw 'check failed: ' + fileError
    
    chooseItem = (items) ->
        items[Math.floor Math.random() * items.length]

    # based on the 20 answers inside a Magic 8 Ball
    checks = JSON.parse jsonData
    result = chooseItem (check for check of checks)
    reason = chooseItem checks[result]
    cb(result, reason)

server.formatResponse = (result, reason, formats) ->
    [any, json] = ['*/*', 'application/json']
    if json in formats 
        [json, JSON.stringify({result: result, reason: reason})]
    else
        [any, "#{result}: #{reason}"]

server.writeResponse = (res, code, contentType, content) ->
    console.log code, content
    res.writeHead code,
        'Content-Type': contentType
        'Content-Length': content.length + 1
    res.write content + '\n'
    res.end()    

server.listen 8000