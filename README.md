Cryptic Compliance Service
--------------------------

An oversight service that allows, holds and blocks actions based on amazing psychic predictions.  Upon calling the service, an answer is returned consisting of a result and a reason.

Results:

* allow
* block
* hold

Reasons: the same equally infallible responses you may have seen the last time you gently shook a [magic 8 ball](http://en.wikipedia.org/wiki/Magic_8-Ball).

Usage
-----

    npm install
    npm start
    curl http://localhost:8000/check